
#include <iostream>
#include "piggy_bank.h"

int main()
{
	piggy_bank pb;
	long long input = 0;
	while (true)
	{
		printf("잔액 : %d\n", pb.remain());
		printf("저금(+)/출금(-) 할 금액 (단.0은 나가기): ");
		scanf("%ld", &input);
		if (input == 0)
		{
			goto EXIT;
		}
		pb.Editing(input);

	}
EXIT:
	return 0;
}