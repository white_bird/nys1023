﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp1
{
    class Program
    {
        static int player_x = 0;
        static int player_y = 0;


        static void SetLoc(int p_x, int p_y)
        {
            Console.SetCursorPosition(p_x * 2, p_y);
        }

        static void PrtPlayer()
        {
            SetLoc(player_x, player_y);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("♥");
        }
        static void PrtBlock(int p_x,int p_y)
        {
            SetLoc(p_x, p_y);
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.Write("■");
        }
        static void PlayerController()
        {

            string tempstr = "abc";


            PrtPlayer();
            if (Console.KeyAvailable)
            {

                ConsoleKeyInfo key = Console.ReadKey();
                if (key.Key == ConsoleKey.UpArrow)
                {
                    if (player_y > 0)
                    {
                        SetLoc(player_x, player_y - 1);
                        if (Console.Read() == ' ') 
                        {
                            SetLoc(player_x, player_y);
                            Console.Write("  ");
                            --player_y;
                            PrtPlayer();
                        }
                    }
                }

                if (key.Key == ConsoleKey.DownArrow)
                {
                    if (player_y < 61)
                    {
                        SetLoc(player_x, player_y + 1);
                        if (Console.Read() == ' ')
                        {
                            SetLoc(player_x, player_y);
                            Console.Write("  ");
                            ++player_y;
                            PrtPlayer();
                        }               
                    }
                }

                if (key.Key == ConsoleKey.LeftArrow)
                {
                    if (player_x > 0)
                    {
                        SetLoc(player_x - 1, player_y);
                        if (Console.Read() == ' ')
                        {
                            SetLoc(player_x, player_y);
                            Console.Write("  ");
                            --player_x;
                            PrtPlayer();
                        }                       
                    }
                }

                if (key.Key == ConsoleKey.RightArrow)
                {
                    if (player_x < 39) 
                    {
                        SetLoc(player_x + 1, player_y);
                        if (Console.Read() == ' ')
                        {
                            SetLoc(player_x, player_y);
                            Console.Write("  ");
                            ++player_x;
                            PrtPlayer();
                        }                   
                    }
                }


            }
        }

        static void Main()
        {
            Console.Title = "Game !";
            Console.CursorVisible = false;
            while (true)
            {
                PlayerController();
            }
            Console.ReadLine();
        }
    }
}
