#include "Hero.h"
#include "Act.h"
#include <iostream>



Hero::Hero(char* name, int hp, int mp, int level, int armor, int speed, int damage, int type, int critical)
{
	own = { {0},hp,mp,level,armor,speed,damage,type,critical,hp,mp };
	strcpy(own.name, name);
}


Hero::~Hero()
{
}

void Hero::LevelUpDown(int a)
{
	own.level_ = own.level_ + a;
}

void Hero::StatShow()
{
	std::cout << "Lv." << own.level_ << "HP : " << own.hp_ << "/" << own.max_hp_ << "MP : " << own.mp_ << "/" << own.max_mp_;
}
