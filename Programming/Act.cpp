
#include "Act.h"
#include "Hero.h"
#include <cstdlib>
#include <ctime>



Act::Act()
{
	srand((unsigned int)time(0));
}


Act::~Act()
{
}

void Act::attack(Hero a, Hero b) //a - b =   2:b   1:a   -1:b   -2:a
{
	if (a.own.attack_type_ - b.own.attack_type_ == 0)  //type 동일
	{
		if (rand() % 100 > b.own.speed_ - a.own.speed_)    //회피
		{
			if (rand() % 100 > a.own.attack_critical_percentage_)    //크리티컬 X
			{
				if (a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10 > (b.own.armor_ + b.own.armor_ * b.own.level_ / 10))
				{
					b.own.hp_ = b.own.hp_ - (a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10 - (b.own.armor_ + b.own.armor_ * b.own.level_ / 10));
				}
			}
			else   //크리티컬 O
			{
				if (2 * (a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10) > (b.own.armor_ + b.own.armor_ * b.own.level_ / 10))
				{
					b.own.hp_ = b.own.hp_ - (2 * (a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10) - (b.own.armor_ + b.own.armor_ * b.own.level_ / 10));
				}
			}
		}
	}
	else if (a.own.attack_type_ - b.own.attack_type_ == 2 || a.own.attack_type_ - b.own.attack_type_ == -1)  //type 약세
	{
		if (rand() % 100 > b.own.speed_ - a.own.speed_)    //회피
		{
			if (rand() % 100 > a.own.attack_critical_percentage_)    //크리티컬 X
			{
				if (a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10 > (b.own.armor_ + b.own.armor_ * b.own.level_ / 10))
				{
					b.own.hp_ = b.own.hp_ - ((a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10 - (b.own.armor_ + b.own.armor_ * b.own.level_ / 10))) / 10 * 9;
				}
			}
			else   //크리티컬 O
			{
				if (2 * (a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10) > (b.own.armor_ + b.own.armor_ * b.own.level_ / 10))
				{
					b.own.hp_ = b.own.hp_ - ((2 * (a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10) - (b.own.armor_ + b.own.armor_ * b.own.level_ / 10))) / 10 * 9;
				}
			}
		}
	}
	else if (a.own.attack_type_ - b.own.attack_type_ == 1 || a.own.attack_type_ - b.own.attack_type_ == -2)  //type 우세
	{
		if (rand() % 100 > b.own.speed_ - a.own.speed_)    //회피
		{
			if (rand() % 100 > a.own.attack_critical_percentage_)    //크리티컬 X
			{
				if (a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10 > (b.own.armor_ + b.own.armor_ * b.own.level_ / 10))
				{
					b.own.hp_ = b.own.hp_ - ((a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10 - (b.own.armor_ + b.own.armor_ * b.own.level_ / 10))) / 10 * 11;
				}
			}
			else   //크리티컬 O
			{
				if (2 * (a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10) > (b.own.armor_ + b.own.armor_ * b.own.level_ / 10))
				{
					b.own.hp_ = b.own.hp_ - ((2 * (a.own.attack_damage_ + a.own.attack_damage_ * a.own.level_ / 10) - (b.own.armor_ + b.own.armor_ * b.own.level_ / 10))) / 10 * 11;
				}
			}
		}
	}
}

