#pragma once
class Hero
{
public:
	Hero(char* name, int hp, int mp, int level, int armor, int speed, int damage, int type, int critical);
	virtual ~Hero();
	struct Stat 
	{
		char name[256];
		int max_hp_;
		int max_mp_;
		int level_;
		int armor_;
		int speed_;
		int attack_damage_;
		int attack_type_;             //가위 0, 바위 1, 보 2
		int attack_critical_percentage_;
		int hp_;
		int mp_;
	};
	void LevelUpDown(int a);
	static Stat own;
	void StatShow();



};

