﻿

#include <iostream>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int lock[4000];
int complock[4000];
int corlock[4000];
int beflock[4000];
char inputlock[10000];
char buffer[4];
int lenth = 0;

int Same_Ar(int a[], int b[]) //문자열 비교
{
	int num1 = 0;
	for (int i = 0; i < lenth; i++)
	{
		if (a[i] == b[i])
		{
			num1++;
		}
	}
	if (num1 == lenth)
	{
		return 1;
	}
	else
	{
		return 0;
	}

}

void Copy_Ar(int from[], int to[])  //문자열 복사
{
	int i = 0;
	for (i = 0; i < lenth; i++)
	{
		to[i] = from[i];
	}
}

void Push(int a) //문자열 a만큼 밀기
{
	int buffer1 = 0;
	int buffer2 = 0;
	for (int i1 = 0; i1 < a; i1++)
	{

		buffer1 = complock[lenth - 1];
		complock[lenth - 1] = complock[0];
		for (int i2 = lenth - 1; i2 >= 0; i2--)
		{
			buffer2 = complock[i2 - 1];
			complock[i2 - 1] = buffer1;
			buffer1 = buffer2;
		}
	}
}

void Flip(int a, int b) //문자열 a번째부터 b번째까지 뒤집기
{
	int buffer1 = 0;
	int buffer2 = 0;
	if (b - a == 1)
	{
		buffer1 = complock[a - 1];
		buffer2 = complock[b - 1];
		complock[a - 1] = buffer2;
		complock[b - 1] = buffer1;
	}
	else if (b - a > 1)
	{
		for (int i1 = 0; i1 < b - a - 1; i1++)
		{
			buffer1 = complock[a + i1 - 1];
			buffer2 = complock[b - i1 - 1];
			complock[a + i1 - 1] = buffer2;
			complock[b - i1 - 1] = buffer1;
		}
	}
}

int main()
{

	int num1 = 0;
	int num2 = 0;
	scanf("%d\n", &lenth);
	gets_s(inputlock);
	for (int i = 0; i < lenth; i++)
	{
		lock[i] = i + 1;
	}

	int num = strlen(inputlock);
	for (int i = 0; i <= num; i++)
	{
		if (inputlock[i] == ' ' || inputlock[i] == '\0')
		{
			corlock[num1] = atoi(buffer);
			buffer[0] = '\0'; buffer[1] = '\0'; buffer[2] = '\0'; buffer[3] = '\0';
			num2 = 0;
			num1++;
		}
		else
		{
			buffer[num2] = inputlock[i];
			num2++;
		}
	}
	for (int i1 = 1; i1 <= lenth; i1++) //주어진 자물쇠와 같은 결과가 나오는 밀기와 뒤집기값 구하기
	{
		for (int i2 = 1; i2 <= lenth; i2++)
		{

			for (int i3 = i2 + 1; i3 <= lenth; i3++)
			{
				for (int i4 = 1; i4 <= lenth; i4++)
				{
					Copy_Ar(lock, complock);
					Push(i1);
					Flip(i2, i3);
					Push(i4);
					if (Same_Ar(complock, corlock))
					{
						printf("%d\n%d %d\n%d\n", i1, i2, i3, i4); //일치하는 밀기와 뒤집기값 출력후 탈출
						goto EXIT;
					}
				}
			}
		}
	}
EXIT:
	return 0;
}
