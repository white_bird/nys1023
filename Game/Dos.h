#pragma once
#define DOSX 79
#define DOSY 59

class Dos
{
	char dos[DOSY][DOSX];
public:
	const char PLAYER = 'P';
	const char VOID = 'V';
	const char BLOCK = 'B';
    void DosCreate();
	void DosPrint();

};

