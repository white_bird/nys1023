#include <iostream>
#include "Dos.h"


void Dos::DosCreate()
{
	for (int i1 = 0; i1 < DOSY; ++i1)
	{
		for (int i2 = 0; i2 < DOSX; ++i2)
		{
			if (i2 == 0 || i2 == DOSY || i1 == 0 || i1 == DOSX)
			{
				dos[i1][i2] = BLOCK;
			}
			else
			{
				dos[i1][i2] = VOID;
			}
		}
	}

}

void Dos::DosPrint()
{
	for (int i1 = 0; i1 < DOSY; i1++)
	{
		for (int i2 = 0; i2 < DOSX; i2++)
		{
			std::cout << dos[i1][i2];
		}
		std::cout << "\n";
	}
}
